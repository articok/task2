using System;
using Xunit;

namespace Task2.Tests
{
    public class GeneratorTest
    {
        [Theory]
        [InlineData(10, 10, 10)]
        [InlineData(20, 10, 50)]
        [InlineData(5, 10, 15)]
        [InlineData(100, 100, 1)]
        public void MinesGenerationV1CorrectCount(int width, int height, int mines)
        {
            var game = new Minesweeper(width, height, mines);
            game.GenerateField();

            var actualMines = 0;

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (game.Field[i, j].Type == CellType.Mine) actualMines++;
                }
            }

            Assert.Equal(mines, actualMines);
        }

        [Theory]
        [InlineData(10, 10, 10)]
        [InlineData(20, 10, 50)]
        [InlineData(5, 10, 15)]
        [InlineData(100, 100, 1)]
        public void MinesGenerationV2CorrectCount(int width, int height, int mines)
        {
            var game = new Minesweeper(width, height, mines);
            game.GenerateField2();

            var actualMines = 0;

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (game.Field[i, j].Type == CellType.Mine) actualMines++;
                }
            }

            Assert.Equal(mines, actualMines);
        }
    }
}
