﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Task2.Tests
{
    public class ClueCalculationTest
    {
        [Theory]
        [InlineData(10, 10, 10)]
        [InlineData(20, 10, 50)]
        [InlineData(5, 10, 15)]
        [InlineData(100, 100, 1)]
        public void ClueCalculationCorrectnessForGeneratorV1(int width, int height, int mines)
        {
            var game = new Minesweeper(width, height, mines);
            game.GenerateField();

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (game.Field[i, j].Type == CellType.Mine)
                        continue;

                    var realMinesInArea = 0;

                    if (DimensionsInBounds(i - 1, j - 1, height, width))
                        if (game.Field[i - 1, j - 1].Type == CellType.Mine)
                            realMinesInArea++;
                    if (DimensionsInBounds(i, j - 1, height, width))
                        if (game.Field[i, j - 1].Type == CellType.Mine)
                            realMinesInArea++;
                    if (DimensionsInBounds(i + 1, j - 1, height, width))
                        if (game.Field[i + 1, j - 1].Type == CellType.Mine)
                            realMinesInArea++;
                    if (DimensionsInBounds(i - 1, j, height, width))
                        if (game.Field[i - 1, j].Type == CellType.Mine)
                            realMinesInArea++;
                    if (DimensionsInBounds(i + 1, j, height, width))
                        if (game.Field[i + 1, j].Type == CellType.Mine)
                            realMinesInArea++;
                    if (DimensionsInBounds(i - 1, j + 1, height, width))
                        if (game.Field[i - 1, j + 1].Type == CellType.Mine)
                            realMinesInArea++;
                    if (DimensionsInBounds(i, j + 1, height, width))
                        if (game.Field[i, j + 1].Type == CellType.Mine)
                            realMinesInArea++;
                    if (DimensionsInBounds(i + 1, j + 1, height, width))
                        if (game.Field[i + 1, j + 1].Type == CellType.Mine)
                            realMinesInArea++;

                    Assert.Equal(game.Field[i, j].MinesInArea, realMinesInArea);
                }
            }
        }

        [Theory]
        [InlineData(10, 10, 10)]
        [InlineData(20, 10, 50)]
        [InlineData(5, 10, 15)]
        [InlineData(100, 100, 1)]
        public void ClueCalculationCorrectnessForGeneratorV2(int width, int height, int mines)
        {
            var game = new Minesweeper(width, height, mines);
            game.GenerateField2();

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (game.Field[i, j].Type == CellType.Mine)
                        continue;

                    var realMinesInArea = 0;

                    if (DimensionsInBounds(i - 1, j - 1, height, width))
                        if (game.Field[i - 1, j - 1].Type == CellType.Mine)
                            realMinesInArea++;
                    if (DimensionsInBounds(i, j - 1, height, width))
                        if (game.Field[i, j - 1].Type == CellType.Mine)
                            realMinesInArea++;
                    if (DimensionsInBounds(i + 1, j - 1, height, width))
                        if (game.Field[i + 1, j - 1].Type == CellType.Mine)
                            realMinesInArea++;
                    if (DimensionsInBounds(i - 1, j, height, width))
                        if (game.Field[i - 1, j].Type == CellType.Mine)
                            realMinesInArea++;
                    if (DimensionsInBounds(i + 1, j, height, width))
                        if (game.Field[i + 1, j].Type == CellType.Mine)
                            realMinesInArea++;
                    if (DimensionsInBounds(i - 1, j + 1, height, width))
                        if (game.Field[i - 1, j + 1].Type == CellType.Mine)
                            realMinesInArea++;
                    if (DimensionsInBounds(i, j + 1, height, width))
                        if (game.Field[i, j + 1].Type == CellType.Mine)
                            realMinesInArea++;
                    if (DimensionsInBounds(i + 1, j + 1, height, width))
                        if (game.Field[i + 1, j + 1].Type == CellType.Mine)
                            realMinesInArea++;

                    Assert.Equal(game.Field[i, j].MinesInArea, realMinesInArea);
                }
            }
        }

        private bool DimensionsInBounds(int x, int y, int max_X, int max_Y)
        {
            return x >= 0 && x < max_X && y >= 0 && y < max_Y;
        }
    }
}
