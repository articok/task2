# Minesweeper Generator #

Implementation of minesweeper task

### Language ###

I spent most of my time on .net projects so my choice (C# and .net core 3.1) was simply love to this language.

### Build ###

* dotnet restore
* dotnet build -c Release -o App

### Run ###

In App folder run Task2.exe width height mines.
./Task2.exe 15 15 10

### Implementation ###

I have implemented 2 different methods for mine generation.

* GenerateField
Ramdomly chooses both dimensions in matrix and if it's possible to put mine there decreases mines counter. 

* GenerateField2
Iterates through ever cell of matrix and decides to put mine there based on dynamically changing probability.

Clue generation is done every time a mine is placed by incrementing MinesInArea counter for all 8 fields in range if they are still neutral fields.