﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task2
{
    public class Cell
    {
        public CellType Type { get; set; }
        public int MinesInArea { get; set; }

        public Cell()
        {
            Type = CellType.Neutral;
            MinesInArea = 0;
        }

        public void AddMineInArea()
        {
            if (Type == CellType.Neutral)
                MinesInArea++;
        }
    }
}
