﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task2
{
    public class Minesweeper
    {
        private readonly int Width;
        private readonly int Height;
        private readonly int Mines;

        public Cell[,] Field;

        public Minesweeper(int width, int height, int mines)
        {
            Width = width;
            Height = height;
            Mines = mines;
            Field = new Cell[height, width];
        }

        public void GenerateField()
        {
            InitField();
            var generator = new Random();
            var remainingMines = Mines;

            while (remainingMines > 0)
            {
                var i = generator.Next(Height);
                var j = generator.Next(Width);

                if (Field[i, j].Type == CellType.Mine)
                    continue;

                Field[i, j].Type = CellType.Mine;
                UpdateCellNeighbors(i, j);
                remainingMines--;
            }
        }

        public void GenerateField2()
        {
            InitField();
            var generator = new Random();
            var remainingCells = Height * Width;
            var remainingMines = Mines;

            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    if (generator.Next(remainingCells - remainingMines) < remainingMines)
                    {
                        Field[i, j].Type = CellType.Mine;
                        UpdateCellNeighbors(i, j);
                        remainingMines--;
                    }
                    remainingCells--;
                }
            }
        }

        public void PrintField()
        {
            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    Console.Write("|");
                    Console.Write(
                        Field[i, j].Type == CellType.Mine ? "X" :
                        Field[i, j].MinesInArea == 0 ? " " : Field[i, j].MinesInArea.ToString());

                }
                Console.WriteLine("|");
            }
        }

        private void InitField()
        {
            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    Field[i, j] = new Cell();
                }
            }
        }

        private void UpdateCellNeighbors(int i, int j)
        {
            if (DimensionsInBounds(i - 1, j - 1, Height, Width))
                Field[i - 1, j - 1].AddMineInArea();
            if (DimensionsInBounds(i, j - 1, Height, Width))
                Field[i, j - 1].AddMineInArea();
            if (DimensionsInBounds(i + 1, j - 1, Height, Width))
                Field[i + 1, j - 1].AddMineInArea();
            if (DimensionsInBounds(i - 1, j, Height, Width))
                Field[i - 1, j].AddMineInArea();
            if (DimensionsInBounds(i + 1, j, Height, Width))
                Field[i + 1, j].AddMineInArea();
            if (DimensionsInBounds(i - 1, j + 1, Height, Width))
                Field[i - 1, j + 1].AddMineInArea();
            if (DimensionsInBounds(i, j + 1, Height, Width))
                Field[i, j + 1].AddMineInArea();
            if (DimensionsInBounds(i + 1, j + 1, Height, Width))
                Field[i + 1, j + 1].AddMineInArea();
        }

        private  bool DimensionsInBounds(int x, int y, int max_X, int max_Y)
        {
            return x >= 0 && x < max_X && y >= 0 && y < max_Y;
        }
    }
}
