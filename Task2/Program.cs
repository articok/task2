﻿using System;
using System.Data;
using System.Linq;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = ParseArgs(args);

            var game = new Minesweeper(input.width, input.height, input.mines);
            game.GenerateField2();
            game.PrintField();
        }

        static (int width, int height, int mines) ParseArgs(string[] args)
        {
            if (args.Count() < 3)
            {
                Console.WriteLine("Please enter width, height and mines count.");
                Environment.Exit(0);
            }
            
            if(!int.TryParse(args[0], out int width))
            {
                Console.WriteLine("Please enter width as positive integer.");
                Environment.Exit(0);
            }

            if (!int.TryParse(args[1], out int height))
            {
                Console.WriteLine("Please enter height as positive integer.");
                Environment.Exit(0);
            }

            if (!int.TryParse(args[2], out int mines))
            {
                Console.WriteLine("Please enter width as positive integer.");
                Environment.Exit(0);
            }

            if ((width * height) < mines )
            {
                Console.WriteLine("Too many mines.");
                Environment.Exit(0);
            }

            return (width, height, mines);
        }
    }  
}
